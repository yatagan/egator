-module(egator).

-export([create/4,destroy/2]).
-export([list/1,list/2]).

-define(GATOR_HOST, "localhost").
-define(GATOR_PORT, 4387).

-define(GTR_MAGIC1, 16#27182818).
-define(GTR_MAGIC2, 16#81828172).

-define(GTR_TAG_ACTION,	5).		%% u32
-define(GTR_TAG_KERNEL,	10).	%% s16
-define(GTR_TAG_NAME,	11).	%% s16
-define(GTR_TAG_MEMORY,	12).	%% u32
-define(GTR_TAG_EXTRA,	13).	%% s16
-define(GTR_TAG_BRIDGE,	14).	%% s16
-define(GTR_TAG_NOVIF,	15).	%%
-define(GTR_TAG_DISK, 16).		%% s16
-define(GTR_TAG_RDONLY, 17).	%%
-define(GTR_TAG_CONSOLE, 18).	%%
-define(GTR_TAG_PREFIX, 20).	%% s16

-define(GTR_TAG_OK, 100).		%%
-define(GTR_TAG_ERROR, 110).	%% u32
-define(GTR_TAG_DOMAINS, 120).	%% u32 (s16 u32)*

-define(GTR_ACTION_CREATE, 1).
-define(GTR_ACTION_CLEANUP, 2).
-define(GTR_ACTION_LIST, 3).

-define(GTR_DOMAIN_RUNNING, 1).
-define(GTR_DOMAIN_BLOCKED, 2).
-define(GTR_DOMAIN_PAUSED, 4).
-define(GTR_DOMAIN_SHUTDOWN, 8).
-define(GTR_DOMAIN_DYING, 16).

create(Name, Image, DomainOpts, GatorOpts)
		when is_binary(Name), is_binary(Image),
			is_list(DomainOpts), is_list(GatorOpts) -> %% ok | {console,Sock} | {error,Error}
	
	EncOpts =
		[<<?GTR_TAG_ACTION:16,?GTR_ACTION_CREATE:32>>,
		 <<?GTR_TAG_NAME:16,(size(Name)):16,Name/binary>>,
		 <<?GTR_TAG_KERNEL:16,(size(Image)):16,Image/binary>>]

	 	++

		lists:map(fun({memory,Memory}) when is_integer(Memory) ->
				<<?GTR_TAG_MEMORY:16,Memory:32>>;
			({extra,Extra}) when is_binary(Extra) ->
				<<?GTR_TAG_EXTRA:16,(size(Extra)):16,Extra/binary>>;
			({bridge,Bridge}) when is_binary(Bridge) ->
				<<?GTR_TAG_BRIDGE:16,(size(Bridge)):16,Bridge/binary>>;
			(novif) ->
				<<?GTR_TAG_NOVIF:16>>;
			({disk,Disk}) when is_binary(Disk) ->
				<<?GTR_TAG_DISK:16,(size(Disk)):16,Disk/binary>>;
			(rdonly) ->
				<<?GTR_TAG_RDONLY:16>>;
			(console) ->
				<<?GTR_TAG_CONSOLE:16>>
		end, DomainOpts),
	
	EncOptsBin = list_to_binary(EncOpts),
	case lists:member(console, DomainOpts) of
	true ->
		gator_request(EncOptsBin, [keep|GatorOpts]);
	false ->
		gator_request(EncOptsBin, GatorOpts)
	end;

create(_, _, _, _) ->
	{error,badarg}.

destroy(Name, GatorOpts)
		when is_binary(Name), is_list(GatorOpts) -> %% ok | {error,Error}

	EncOpts =
		[<<?GTR_TAG_ACTION:16,?GTR_ACTION_CLEANUP:32>>,
		 <<?GTR_TAG_NAME:16,(size(Name)):16,Name/binary>>],
	 
	EncOptsBin = list_to_binary(EncOpts),
	gator_request(EncOptsBin, GatorOpts);

destroy(_, _) ->
	{error,badarg}.

list(GatorOpts) when is_list(GatorOpts) ->
	EncOpts =
		[<<?GTR_TAG_ACTION:16,?GTR_ACTION_LIST:32>>],

	 EncOptsBin = list_to_binary(EncOpts),
	 list_request(EncOptsBin, GatorOpts);

list(_) ->
	{error,badarg}.

list(Prefix, GatorOpts) when is_binary(Prefix), is_list(GatorOpts) ->
	EncOpts =
		[<<?GTR_TAG_ACTION:16,?GTR_ACTION_LIST:32>>,
		 <<?GTR_TAG_PREFIX:16,(size(Prefix)):16,Prefix/binary>>],

	 EncOptsBin = list_to_binary(EncOpts),
	 list_request(EncOptsBin, GatorOpts);

list(_, _) ->
	{error,badarg}.

%%------------------------------------------------------------------------------

list_request(EncOptsBin, GatorOpts) ->
	case gator_request(EncOptsBin, GatorOpts) of
	{ok,<<?GTR_TAG_DOMAINS:16,NumDoms:32,DomsBin/binary>>} ->
		parse_domain_list(NumDoms, DomsBin);
	Other ->
		Other
	end.

parse_domain_list(N, DomsBin) ->
	parse_domain_list(N, DomsBin, []).

parse_domain_list(0, <<>>, Acc) ->
	{ok,lists:reverse(Acc)};
parse_domain_list(N, <<Len:16,Name:Len/binary,StateBits:32,DomsBin/binary>>, Acc) ->

	Spec = [{?GTR_DOMAIN_RUNNING,running},
			{?GTR_DOMAIN_BLOCKED,blocked},
			{?GTR_DOMAIN_PAUSED,paused},
			{?GTR_DOMAIN_SHUTDOWN,shutdown},
			{?GTR_DOMAIN_DYING,dying}],

	States = lists:foldl(fun({Mask,State}, States)
		when (StateBits band Mask) =/= 0 ->
			[State|States];
		(_, States) ->
			States
	end, [], Spec),

	parse_domain_list(N -1, DomsBin, [{Name,States}|Acc]).

gator_request(EncOptsBin, GatorOpts) ->
	TotalSize = size(EncOptsBin) + 8,
	Request = <<?GTR_MAGIC1:32,TotalSize:32,EncOptsBin/binary>>,

	GatorHost = proplists:get_value(host, GatorOpts, ?GATOR_HOST),
	GatorPort = proplists:get_value(port, GatorOpts, ?GATOR_PORT),

	case gen_tcp:connect(GatorHost, GatorPort, [{active,false},binary]) of
	{ok,Sock} ->
		ok = gen_tcp:send(Sock, Request),

		case gen_tcp:recv(Sock, 8) of
		{ok,<<?GTR_MAGIC2:32,RespSize:32>>} ->
			case gen_tcp:recv(Sock, RespSize -8) of
			{ok,<<?GTR_TAG_OK:16>>} ->
				case lists:member(keep, GatorOpts) of
					true -> {console,Sock};
					false -> gen_tcp:close(Sock), ok end;
			{ok,<<?GTR_TAG_ERROR:16,GatorError:32>>} ->
				gen_tcp:close(Sock),
				{error,{gator_code,GatorError}};
			{ok,Data} ->
				gen_tcp:close(Sock),
				{ok,Data};
			Error ->
				gen_tcp:close(Sock),
				Error
			end;
		Error ->
			gen_tcp:close(Sock),
			Error
		end;
	{error,_} = Error ->
		Error
	end.

%%EOF
